package avltree

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// Run from terminal:
// go test -v -timeout 30s -run ^TestAVLTreePreOrder$ gitlab.com/jeffreylane/algorithms/avl-tree
func TestAVLTreePreOrder(t *testing.T) {
	t.Log("Creating BST with []int{10, 32, 2, 5, 8, 17, 54, 42}")
	t.Log(
		`
              10
		`)
	time.Sleep(2500 * time.Millisecond)
	t.Log(
		`Balanced
              10
                \
                 32 
	`)
	time.Sleep(2500 * time.Millisecond)
	t.Log(`Balanced
              10
             /  \
            2    32 
	`)
	time.Sleep(2500 * time.Millisecond)
	t.Log(`Balanced
              10
             /  \
            2    32 
             \
              5
	`)
	time.Sleep(2500 * time.Millisecond)
	t.Log(`Unbalanced. Need RR Rotation.
              10
             /  \
            2    32 
             \
              5
               \
                8
	`)
	time.Sleep(2500 * time.Millisecond)
	t.Log(`Balanced by RR Rotation.
              10
             /  \
            5    32 
           / \
          2   8
	`)
	time.Sleep(2500 * time.Millisecond)
	t.Log(`Balanced.
              10
             /  \
            5    32 
           / \   /
          2   8 17
	`)
	time.Sleep(2500 * time.Millisecond)
	t.Log(`Balanced.
              10
             /  \
            5    32 
           / \   / \
          2   8 17  54
	`)
	time.Sleep(2500 * time.Millisecond)
	t.Log(`Balanced.
              10
             /  \
            5    32 
           / \   / \
          2   8 17  54
                    /
                   42
	`)

	nodeValues := []int{10, 32, 2, 5, 8, 17, 54, 42}

	tree := NewAVLTree(nodeValues[0])
	lessThan := func(a, b int) bool {
		return a < b
	}
	greaterThan := func(a, b int) bool {
		return a > b
	}
	for i := 1; i < len(nodeValues); i++ {
		tree = Insert(nodeValues[i], tree, lessThan, greaterThan)
	}
	expected := []int{10, 5, 2, 8, 32, 17, 54, 42}
	actual := preOrder(tree)
	if assert.Equalf(t, expected, actual, "Expected %v, Got %v", expected, actual) {
		t.Log("Success!")
	}

}
func preOrder(n *AVLTree[int]) (result []int) {
	if n != nil {
		result = append(result, n.Value)
		left := preOrder(n.Left)
		result = append(result, left...)
		right := preOrder(n.Right)
		result = append(result, right...)
	}
	return
}
