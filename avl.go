package avltree

type AVLTree[T any] struct {
	Value       T
	Height      int
	Left, Right *AVLTree[T]
}

func NewAVLTree[T any](value T) *AVLTree[T] {
	return &AVLTree[T]{
		Value:  value,
		Height: 1,
	}
}

// Inserts a node into the balanced binary search tree, and ensures it remains balanced.
// Example of lessThan and greaterThan func assuming underlying type of T is int:
//
//	lessThan := func(a, b int) bool {
//		return a < b
//	}
//
//	greaterThan := func(a, b int) bool {
//		return a > b
//	}
func Insert[T any](value T, node *AVLTree[T], lessThan, greaterThan func(T, T) bool) *AVLTree[T] {
	if node == nil {
		return NewAVLTree(value)
	}

	if lessThan(value, node.Value) {
		node.Left = Insert(value, node.Left, lessThan, greaterThan)
	} else if greaterThan(value, node.Value) {
		node.Right = Insert(value, node.Right, lessThan, greaterThan)
	} else {
		// duplicate values not allowed
		return node
	}
	// Update height of this parent node
	node.Height = max(height(node.Left), height(node.Right)) + 1
	// Check if node became unbalanced
	balance := getBalance(node)

	// If node is unbalanced, then are 4 cases
	// Left Left Case
	if balance > 1 && lessThan(value, node.Left.Value) {
		return rightRotate(node)
	}
	// Right Right Case
	if balance < -1 && greaterThan(value, node.Right.Value) {
		return leftRotate(node)
	}

	// Left Right Case
	if balance > 1 && greaterThan(value, node.Right.Value) {
		node.Left = leftRotate(node.Left)
		return rightRotate(node)
	}

	// Right Left Case
	if balance < -1 && lessThan(value, node.Right.Value) {
		node.Right = rightRotate(node.Right)
		return leftRotate(node)
	}
	// return unchanged pointer
	return node
}

func height[T any](node *AVLTree[T]) int {
	if node == nil {
		return 0
	}
	return node.Height
}

func getBalance[T any](node *AVLTree[T]) int {
	if node == nil {
		return 0
	}
	return height(node.Left) - height(node.Right)
}

func rightRotate[T any](y *AVLTree[T]) *AVLTree[T] {
	x := y.Left
	z := x.Right
	// swap
	x.Right = y
	y.Left = z
	// update heights
	y.Height = max(height(y.Left), height(y.Right)) + 1
	x.Height = max(height(x.Left), height(x.Right)) + 1
	// return new root
	return x
}

func leftRotate[T any](x *AVLTree[T]) *AVLTree[T] {
	y := x.Right
	z := y.Left
	// swap
	y.Left = x
	x.Right = z
	// update heights
	x.Height = max(height(x.Left), height(x.Right)) + 1
	y.Height = max(height(y.Left), height(y.Right)) + 1
	// return new root
	return y
}
