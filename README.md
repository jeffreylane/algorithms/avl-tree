# avl-tree
An implementation of a balanced Binary Search Tree based on the AVL Tree Algorithm using Go Generics.
Props to Georgy Adelson-Velsky and Evgenii Landis.